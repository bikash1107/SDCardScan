package com.bik.scansdcard.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.BuildConfig;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.bik.scansdcard.R;
import com.bik.scansdcard.utils.Utils;

public class SDCardActivity extends AppCompatActivity {

    private static final String TAG = SDCardActivity.class.getSimpleName();
    String tag = "sdcardfragment";
    FragmentManager mFragmentManager = null;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 200;
    private static String[] permissionNeed = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sdcard_main);
//        Utils.checkPermission(permissionNeed, this);
        loadFragment();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Utils.checkPermissionGranted(this , Manifest.permission.READ_EXTERNAL_STORAGE )) {
            requestPermissions();

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void loadFragment()
    {
        mFragmentManager = getSupportFragmentManager();
        SDCardFragment sdFragment = (SDCardFragment) mFragmentManager.findFragmentByTag(tag);

        // If the Fragment is non-null, then it is currently being
        // retained across a configuration change.
        if (sdFragment == null) {
            sdFragment = new SDCardFragment();
            mFragmentManager.beginTransaction().add(R.id.container_sd_activity, sdFragment, tag).commit();
        }

        Log.d(TAG, "loadFragment() -> All Layout loaded ");
    }


    @Override
    public void onBackPressed() {
        final SDCardFragment sdFragment = (SDCardFragment) mFragmentManager.findFragmentByTag(tag);
        if (!sdFragment.allowBackPressed()) {
            super.onBackPressed();
        }else  if(sdFragment != null){
            sdFragment.onBackPressed();
        }
    }



    public void requestPermissions() {

        boolean shouldProvideRationale =

                ActivityCompat.shouldShowRequestPermissionRationale(this,

                        Manifest.permission.READ_EXTERNAL_STORAGE);



        // Provide an additional rationale to the user. This would happen if the user denied the

        // request previously, but didn't check the "Don't ask again" checkbox.

        if (shouldProvideRationale) {

            Log.i(TAG, "Displaying permission rationale to provide additional context.");

            showSnackbar(R.string.permission_storage,

                    android.R.string.ok, new View.OnClickListener() {

                        @Override

                        public void onClick(View view) {

                            // Request permission

                            ActivityCompat.requestPermissions(SDCardActivity.this,

                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},

                                    REQUEST_PERMISSIONS_REQUEST_CODE);

                        }

                    });

        } else {

            Log.i(TAG, "Requesting permission");

            // Request permission. It's possible this can be auto answered if device policy

            // sets the permission in a given state or the user denied the permission

            // previously and checked "Never ask again".

            ActivityCompat.requestPermissions(SDCardActivity.this,

                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},

                    REQUEST_PERMISSIONS_REQUEST_CODE);

        }

    }
    /**

     * Shows a {@link Snackbar}.

     *

     * @param mainTextStringId The id for the string resource for the Snackbar text.

     * @param actionStringId   The text of the action item.

     * @param listener         The listener associated with the Snackbar action.

     */

    private void showSnackbar(final int mainTextStringId, final int actionStringId,

                              View.OnClickListener listener) {

        Snackbar.make(

                findViewById(android.R.id.content),

                getString(mainTextStringId),

                Snackbar.LENGTH_INDEFINITE)

                .setAction(getString(actionStringId), listener).show();

    }




    /**

     * Callback received when a permissions request has been completed.

     */

    @Override

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,

                                           @NonNull int[] grantResults) {

        Log.i(TAG, "onRequestPermissionResult");

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {

            if (grantResults.length <= 0) {

                // If user interaction was interrupted, the permission request is cancelled and you

                // receive empty arrays.

                Log.i(TAG, "User interaction was cancelled.");

            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Permission granted, updates requested, starting SD card Scanning");


            } else {

                // Permission denied.


                // Notify the user via a SnackBar that they have rejected a core permission for the

                // app, which makes the Activity useless. In a real app, core permissions would

                // typically be best requested during a welcome-screen flow.


                // Additionally, it is important to remember that a permission might have been

                // rejected without asking the user for permission (device policy or "Never ask

                // again" prompts). Therefore, a user interface affordance is typically implemented

                // when permissions are denied. Otherwise, your app could appear unresponsive to

                // touches or interactions which have required permissions.

            requestPermissions();
            }
        }
    }





}
