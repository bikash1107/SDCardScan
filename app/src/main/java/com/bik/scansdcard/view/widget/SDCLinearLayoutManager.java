package com.bik.scansdcard.view.widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;


/**
 * Created by Bikash.K on 3/30/2016.
 * Custom Linear layout to handle the scrolling .
 */
public class SDCLinearLayoutManager extends LinearLayoutManager {
    private boolean isScrollEnabled = true;
    private static final String TAG = SDCLinearLayoutManager.class.getName();



    public SDCLinearLayoutManager(Context context) {
        super(context);
    }

    public void setScrollEnabled(boolean flag) {
        this.isScrollEnabled = flag;
    }


    @Override
    public boolean canScrollVertically() {
        //Similarly you can customize "canScrollHorizontally()" for managing horizontal scroll
        //return isScrollEnabled && super.canScrollVertically();
        return isScrollEnabled;
    }


}
