package com.bik.scansdcard.view.adaptor;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bik.scansdcard.R;
import com.bik.scansdcard.model.SDCardFiles;
import com.bik.scansdcard.utils.Utils;

import java.util.ArrayList;
import java.util.Vector;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Bikash
 * Adapter class to list down the NYC school results

 */

public class SDCAdapter extends RecyclerView.Adapter<SDCAdapter.SDCViewHolder> {

    private static final String TAG = SDCAdapter.class.getSimpleName();
    /**
     * Current Context
     */
    private Context mContext;
    private Vector<SDCardFiles> mAdapterItemList = new Vector<>();

    private View mView;

    public SDCAdapter(Activity mContext, Vector<SDCardFiles> aAdapterItemList) {
        super();
        this.mContext = mContext;
        this.mAdapterItemList = aAdapterItemList;
    }

    @Override
    public SDCViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mView = LayoutInflater.from(mContext).inflate(R.layout.sdcard_rcylr_item, parent, false);
        return new SDCViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(SDCViewHolder holder, final int position) {
        Log.d(TAG , "onBindViewHolder" );
        if (holder!=null  && holder instanceof SDCViewHolder) {
            //Data Received from the server

            holder.fileName.setText(" File Name: " + mAdapterItemList.get(position).getFileName() );
            String filesize = "0kB";
            try {
                filesize =Utils.convertToStringRepresentation(mAdapterItemList.get(position).getFilesize());
            }catch (Exception ex) {

            }
            holder.fileSize.setText("File Size:  " + filesize);
        }

    }
    @Override
    public void onViewAttachedToWindow(SDCViewHolder holder) {
        super.onViewAttachedToWindow(holder);

    }
    @Override
    public int getItemCount() {

        return mAdapterItemList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * Update adapter list
     * @param aRecyclerItemList updated result list
     */
    public void updateAdapterList(Vector<SDCardFiles> aRecyclerItemList) {
//        mAdapterItemList.clear();
        mAdapterItemList = aRecyclerItemList;
        notifyDataSetChanged();


    }

    /**
     * View holder for adaptor.
     */
    public class SDCViewHolder extends RecyclerView.ViewHolder {

        /**
         * Binding view holder by the help of butter knife.
         */

        TextView fileName;
        TextView fileSize;

        public SDCViewHolder(View itemView) {
            super(itemView);
//            ButterKnife.bind(this,itemView);
            fileSize = (TextView) itemView.findViewById(R.id.filesize);
            fileName = (TextView) itemView.findViewById(R.id.filename);

        }
    }
}
