package com.bik.scansdcard.view.adaptor;


import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bik.scansdcard.R;
import com.bik.scansdcard.model.SDCFileExten;

import java.util.ArrayList;
import java.util.Vector;


/**
 * Created by Bikash
 * Adapter class to list down the NYC school results

 */

public class SDCFileExtensionAdapter extends RecyclerView.Adapter<SDCFileExtensionAdapter.SDCViewHolder> {

    private static final String TAG = SDCFileExtensionAdapter.class.getSimpleName();
    /**
     * Current Context
     */
    private Context mContext;
    private Vector<SDCFileExten> mAdapterItemList = new Vector<>();

    private View mView;

    public SDCFileExtensionAdapter(Activity mContext, Vector<SDCFileExten> aAdapterItemList) {
        super();
        this.mContext = mContext;
        this.mAdapterItemList = aAdapterItemList;
    }

    @Override
    public SDCViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mView = LayoutInflater.from(mContext).inflate(R.layout.sdcard_rcylr_extn, parent, false);
        return new SDCViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(SDCViewHolder holder, final int position) {
        Log.d(TAG , "onBindViewHolder" );
        if (holder!=null  && holder instanceof SDCViewHolder ) {
            //Data Received from the server

            holder.fileName.setText("File Extension: " +mAdapterItemList.get(position).getExtension() );
            holder.fileSize.setText("Frequency: " +mAdapterItemList.get(position).getFrequency());
        }

    }
    @Override
    public void onViewAttachedToWindow(SDCViewHolder holder) {
        super.onViewAttachedToWindow(holder);

    }
    @Override
    public int getItemCount() {

        return mAdapterItemList.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * Update adapter list
     * @param aRecyclerItemList updated result list
     */
    public void updateAdapterList(Vector<SDCFileExten> aRecyclerItemList) {
//        mAdapterItemList.clear();
        mAdapterItemList = aRecyclerItemList;
        notifyDataSetChanged();


    }

    /**
     * View holder for adaptor.
     */
    public class SDCViewHolder extends RecyclerView.ViewHolder {

        /**
         * Binding view holder by the help of butter knife.
         */

        TextView fileName;
        TextView fileSize;

        public SDCViewHolder(View itemView) {
            super(itemView);
//            ButterKnife.bind(this,itemView);
            fileSize = (TextView) itemView.findViewById(R.id.filesize);
            fileName = (TextView) itemView.findViewById(R.id.filename);

        }
    }
}
