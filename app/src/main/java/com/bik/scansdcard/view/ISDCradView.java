package com.bik.scansdcard.view;

import com.bik.scansdcard.model.SDCardFiles;

import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

public interface ISDCradView {

    public void notifyDataChanged(Vector<SDCardFiles> aSDcardFiles);
    public void setProgress(int aProgress);
}
