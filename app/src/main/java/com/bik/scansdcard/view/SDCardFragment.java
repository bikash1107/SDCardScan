package com.bik.scansdcard.view;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bik.scansdcard.R;
import com.bik.scansdcard.model.SDCFileExten;
import com.bik.scansdcard.model.SDCardFiles;
import com.bik.scansdcard.presenter.IPresenter;
import com.bik.scansdcard.presenter.SDCardPresenterImpl;
import com.bik.scansdcard.utils.Utils;
import com.bik.scansdcard.view.adaptor.SDCAdapter;
import com.bik.scansdcard.view.adaptor.SDCFileExtensionAdapter;
import com.bik.scansdcard.view.widget.SDCLinearLayoutManager;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

public class SDCardFragment extends Fragment implements ISDCradView{

    TextView avagFileSize ;
    RecyclerView fileExtension;
    RecyclerView Files;
    Button startScan,shareStatistics;
    boolean isProcessing = false;
    boolean mStartScan = false;
    IPresenter presenter;
    private SDCAdapter mSDCAdaptor;
    private SDCFileExtensionAdapter mSDCExtensionAdaptor;
    private Vector<SDCardFiles> mRecyclerItemList = new Vector<>();
    private Vector<SDCFileExten> mRecyclerFileExtnList = new Vector<>();
    private static final String TAG = SDCardFragment.class.getSimpleName();
    private ProgressDialog progress;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        presenter = new SDCardPresenterImpl(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void createProgressbar() {
        progress=new ProgressDialog(getContext());
        progress.setMessage("Scanning SDCard...");
        progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progress.setIndeterminate(true);
        progress.setButton(DialogInterface.BUTTON_POSITIVE, "Stop Scanning...", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog,
                                int whichButton){
                Toast.makeText(getContext()
                        ,
                        "Scanning stopped!", Toast.LENGTH_SHORT).show();
                presenter.stopScan();
                dismisProgressBar();

            }
        });
        progress.setCanceledOnTouchOutside(false);
        progress.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                presenter.stopScan();
            }
        });

//        progress.setProgress(0);

    }
    public void showProgressbar() {
        progress.show();
    }
    public void dismisProgressBar() {
        progress.dismiss();
    }
    @Override
    public void setProgress(int aProgress) {
        progress.setProgress(aProgress);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View lView = inflater.inflate(R.layout.sdcard_view, container, false);
        avagFileSize = (TextView) lView.findViewById(R.id.avarage_filesize);
        startScan = (Button) lView.findViewById(R.id.start_stop);
        shareStatistics = (Button) lView.findViewById(R.id.share);
        fileExtension = (RecyclerView) lView.findViewById(R.id.recycler_view_file_extens);
        Files = (RecyclerView) lView.findViewById(R.id.recycler_view);
        createProgressbar();
        prepareRecyclerView();


        return lView;
    }


    private void prepareRecyclerView() {

        shareStatistics.setVisibility(View.GONE);
        startScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(startScan.getText().equals(R.string.stopscan)) {
                    presenter.stopScan();
                }else {
                        presenter.startScan();

                }
                startStopScan(false);



            }
        });
        shareStatistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO Share statistics
                Toast.makeText(getContext(),"Data Shared ",Toast.LENGTH_LONG).show();
            }
        });
        //File Deatils
        mSDCAdaptor = new SDCAdapter(getActivity(), mRecyclerItemList);
        SDCLinearLayoutManager wmLayout = new SDCLinearLayoutManager(getContext());
        Files.setLayoutManager(wmLayout);
        Files.setAdapter(mSDCAdaptor);
        Files.setNestedScrollingEnabled(true);
        Files.setHasFixedSize(true);

        //File extenssion
        mSDCExtensionAdaptor = new SDCFileExtensionAdapter(getActivity(), mRecyclerFileExtnList);
        SDCLinearLayoutManager esxtLayout = new SDCLinearLayoutManager(getContext());
        fileExtension.setLayoutManager(esxtLayout);
        fileExtension.setAdapter(mSDCExtensionAdaptor);
        fileExtension.setNestedScrollingEnabled(true);
        fileExtension.setHasFixedSize(true);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public synchronized void notifyDataChanged(final Vector<SDCardFiles> aSDcardFiles) {


        /**
         * Handler to post the data to UI thread
         */
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if(aSDcardFiles == null) {
                    Toast.makeText(getContext() , " Please allow Read permission to scan your SD card",Toast.LENGTH_LONG).show();
                    return;
                }
                isProcessing = true;
                String filesize = "0kB";
                try {
                    filesize =Utils.convertToStringRepresentation(presenter.getAvarageFileSize());
                }catch (Exception ex) {

                }
                avagFileSize.setText(getString(R.string.avaragefilesize)+":"+ filesize);
                mRecyclerItemList = aSDcardFiles;
                mRecyclerFileExtnList.clear();
                ConcurrentHashMap<String,Integer> lSortedExtens =  presenter.getSortedFileExtension();
                Iterator itrMap = lSortedExtens.entrySet().iterator();
                int i =0;
                while (itrMap .hasNext() ) {
                    Map.Entry entry = (Map.Entry) itrMap.next();
                    SDCFileExten lFileExt = new SDCFileExten();
                    lFileExt.setExtension((String)entry.getKey());
                    lFileExt.setFrequency((int)entry.getValue());
                    mRecyclerFileExtnList.add(lFileExt);
                }
                try {
                    mRecyclerFileExtnList = Utils.sortbyFrequency(mRecyclerFileExtnList);

                    mRecyclerItemList = Utils.sortbyFileSize(mRecyclerItemList);
                }catch (Exception ex) {
                    ex.printStackTrace();
                }

                Vector<SDCardFiles> lOnlyTopTen = null;
                if(mRecyclerItemList.size() > 10) {
                     lOnlyTopTen = new Vector<>(mRecyclerItemList.subList(0, 10));
                }else {
                    lOnlyTopTen = new Vector<>(mRecyclerItemList.subList(0, mRecyclerItemList.size()));
                }

                Vector<SDCFileExten> lOnlyTopFive = null;
                if(mRecyclerFileExtnList.size() > 5) {
                    lOnlyTopFive = new Vector<>(mRecyclerFileExtnList.subList(0, 5));
                }else {
                    lOnlyTopFive = new Vector<>(mRecyclerFileExtnList.subList(0, mRecyclerFileExtnList.size()));
                }


                mSDCAdaptor.updateAdapterList(lOnlyTopTen);
                mSDCAdaptor.notifyDataSetChanged();
                mSDCExtensionAdaptor.updateAdapterList(lOnlyTopFive);
                mSDCExtensionAdaptor.notifyDataSetChanged();
                mStartScan =true;
                startStopScan(true);
                shareStatistics.setVisibility(View.VISIBLE);
                isProcessing = false;
            }
        });
    }

    private void startStopScan(boolean noaction) {
        if(mStartScan) {
            mStartScan = false;
            startScan.setText(R.string.startscan);
           /* if(!noaction) {
                presenter.stopScan();
            }*/
                dismisProgressBar();

        }else {
            mStartScan = true;
            startScan.setText(R.string.stopscan);
            if(!noaction && !isProcessing) {
                shareStatistics.setVisibility(View.GONE);
//                presenter.startScan();
                showProgressbar();
            }else if(isProcessing) {
                Toast.makeText(getContext() , "Scanning is in  process please wait and try again " , Toast.LENGTH_LONG).show();
            }
        }
    }


    public boolean allowBackPressed() {
        return true;
    }
    public void onBackPressed() {
        presenter.stopScan();
    }
}
