package com.bik.scansdcard.model;

public class SDCFileExten {
    private int filesize;
    private String extension;

    public int getFrequency() {
        return filesize;
    }

    public void setFrequency(int filesize) {
        this.filesize = filesize;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
