package com.bik.scansdcard.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.bik.scansdcard.model.SDCFileExten;
import com.bik.scansdcard.model.SDCardFiles;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class Utils {

    private static final long K = 1024;
    private static final long M = K * K;
    private static final long G = M * K;
    private static final long T = G * K;

    public static synchronized String getFileExtension(String aFileName) {
        int startIndex = aFileName.lastIndexOf('.');
        if(startIndex > 0)
        return aFileName.substring(aFileName.lastIndexOf('.') + 1 , aFileName.length() );
        else
            return null;
    }


    public static synchronized String convertToStringRepresentation(final long value){
        final long[] dividers = new long[] { T, G, M, K, 1 };
        final String[] units = new String[] { "TB", "GB", "MB", "KB", "B" };
        if(value < 1)
            throw new IllegalArgumentException("Invalid file size: " + value);
        String result = null;
        for(int i = 0; i < dividers.length; i++){
            final long divider = dividers[i];
            if(value >= divider){
                result = format(value, divider, units[i]);
                break;
            }
        }
        return result;
    }

    private static String format(final long value,
                                 final long divider,
                                 final String unit){
        final double result =
                divider > 1 ? (double) value / (double) divider : (double) value;
        return new DecimalFormat("#,##0.#").format(result) + " " + unit;
    }


    public static synchronized Vector<SDCFileExten> sortbyFrequency(Vector<SDCFileExten> aList) {
        // Sorting
        Collections.sort(aList, new Comparator<SDCFileExten>() {
            @Override
            public int compare(SDCFileExten obj2, SDCFileExten obj1)
            {

                return obj1.getFrequency()- obj2.getFrequency();
            }
        });
        return aList;
    }
    public static synchronized Vector<SDCardFiles> sortbyFileSize(Vector<SDCardFiles> aList) {
        // Sorting
        Collections.sort(aList, new Comparator<SDCardFiles>() {
            @Override
            public int compare(SDCardFiles obj2, SDCardFiles obj1)
            {
                return  Long.compare(obj1.getFilesize(),obj2.getFilesize());
//                return obj1.getFilesize() - obj2.getFilesize();
            }
        });
        return aList;
    }

    public static  void checkPermission(String[] permission,Activity activity)
    {
        for (String lPerm : permission) {
            if (!checkPermissionGranted(activity, lPerm)) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        lPerm)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(activity,
                            permission,
                            1);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
        }
    }
    public static boolean checkPermissionGranted(Activity activity, String permission)
    {
        return ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED;

    }
}
