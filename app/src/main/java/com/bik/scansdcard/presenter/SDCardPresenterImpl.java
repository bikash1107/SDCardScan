package com.bik.scansdcard.presenter;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.bik.scansdcard.model.SDCardFiles;
import com.bik.scansdcard.utils.Utils;
import com.bik.scansdcard.view.ISDCradView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

public class SDCardPresenterImpl implements IPresenter {

    ISDCradView mView;
    private File root;
    private long mTotalFileSize = 0;
    private long mAvgFileSize = 0;
    boolean scanning = false;
    private final static  String TAG = SDCardPresenterImpl.class.getSimpleName();
    private Vector<File> fileList = new Vector<File>();
    private Vector<SDCardFiles> mSDCardFiles = new Vector<SDCardFiles>();

    private ConcurrentHashMap<String , Integer> mFileexteCoun = new ConcurrentHashMap<>();
    public SDCardPresenterImpl (ISDCradView aView) {
        this.mView = aView;

    }
    private AsyncTask scanTask = null;

    @Override
    public void startScan() {
        scanning = true;

        mSDCardFiles.clear();
        mFileexteCoun.clear();
        scanTask = new ScanFilesTask();
        scanTask.execute();
    }

    private synchronized  Vector<SDCardFiles> getfile(File dir) {
        File listFile[] = dir.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {

                if (listFile[i].isDirectory()) {
                    fileList.add(listFile[i]);
                    getfile(listFile[i]);

                } else {


                    String lFileExt = Utils.getFileExtension(listFile[i].getName());
                    Log.d(TAG , "get File extension "+lFileExt);
                    if(Utils.getFileExtension(listFile[i].getName()) != null && mFileexteCoun.get(lFileExt) != null) {
                        mFileexteCoun.put(lFileExt, (int) mFileexteCoun.get(lFileExt) + 1);
                    }
                    else if(lFileExt != null) {
                        mFileexteCoun.put(lFileExt, 1);
                    }


                    mTotalFileSize += listFile[i].length();
                    SDCardFiles cardFile = new SDCardFiles();
                    cardFile.setFileName(listFile[i].getName());
                    cardFile.setExtension(lFileExt);
                    try {
                        cardFile.setFilesize(listFile[i].length());
                    }catch (Exception ex) {
                        ex.printStackTrace();
                        cardFile.setFilesize(1);
                        Log.e(TAG , ex.toString());
                    }
                    mSDCardFiles.add(cardFile);
                    fileList.add(listFile[i]);
//                    }
                }
                if(!scanning)
                    break;

            }
            mAvgFileSize = mTotalFileSize / fileList.size() ;
            Log.d(TAG , "Total size  "+ mTotalFileSize + " Avag Size =" +mAvgFileSize);

        }
        return mSDCardFiles;
    }
    @Override
    public void stopScan() {

        if(scanTask != null) {
            scanTask.cancel(true);
        }
        mView.notifyDataChanged(mSDCardFiles);
        scanning = false;

    }

    private class ScanFilesTask extends AsyncTask<Object, Integer, Vector<SDCardFiles>> {

        protected void onProgressUpdate(Integer... progress) {
            //TODO Progress bar
            mView.setProgress((int)progress[0]);
        }

        @Override
        protected Vector<SDCardFiles> doInBackground(Object... objects) {
            Vector<SDCardFiles> files = null;
            if(!isCancelled()) {
                try {
                root = new File(Environment.getExternalStorageDirectory()
                        .getAbsolutePath());
                    files = getfile(root);
                }catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            return files;
        }

        protected void onPostExecute(Vector<SDCardFiles> result) {
            if(scanning) {
                mView.notifyDataChanged(result);
            }
            scanning = false;
            scanTask = null;
        }
    }

    @Override
    public long getAvarageFileSize() {
        return mAvgFileSize;
    }

    @Override
    public ConcurrentHashMap<String, Integer> getSortedFileExtension() {
        return mFileexteCoun;
    }

    @Override
    public Vector<SDCardFiles> getSortedFileSize() {
        return null;
    }
}
