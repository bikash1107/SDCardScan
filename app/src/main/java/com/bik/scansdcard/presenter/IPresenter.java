package com.bik.scansdcard.presenter;

import com.bik.scansdcard.model.SDCardFiles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

public interface IPresenter {

    public void startScan();
    public void stopScan();
    public long getAvarageFileSize();
    public ConcurrentHashMap<String,Integer> getSortedFileExtension();
    public Vector<SDCardFiles> getSortedFileSize();

}
